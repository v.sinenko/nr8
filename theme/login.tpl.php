<style>
* {
  box-sizing: border-box;
}

#infinity {
  position: relative;
  width: 212px;
  height: 100px;
  left: 280%;
  top: 100px;
}

#infinity:before,
#infinity:after {
  content: "";
  position: absolute;
  top: 0;
  left: 0;
  width: 60px;
  height: 60px;    
  border: 20px solid rgb(255, 255, 255);
  -moz-border-radius: 50px 50px 0 50px;
       border-radius: 50px 50px 0 50px;
  -webkit-transform: rotate(-45deg);
     -moz-transform: rotate(-45deg);
      -ms-transform: rotate(-45deg);
       -o-transform: rotate(-45deg);
          transform: rotate(-45deg);
}

#infinity:after {
  left: auto;
  right: 0;
  -moz-border-radius: 50px 50px 50px 0;
       border-radius: 50px 50px 50px 0;
  -webkit-transform:rotate(45deg);
     -moz-transform:rotate(45deg);
      -ms-transform:rotate(45deg);
       -o-transform:rotate(45deg);
          transform:rotate(45deg);
}

#heart {
  position: relative;
  width: 100px;
  height: 90px;
  left: 300%;
  top: -250px;
}

#heart:before, #heart:after {
  position: absolute;
  content: "";
  left: 50px;
  top: 0;
  width: 50px;
  height: 80px;
  background: white;
  -moz-border-radius: 50px 50px 0 0;
  border-radius: 50px 50px 0 0;
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transform-origin: 0 100%;
  -moz-transform-origin: 0 100%;
  -ms-transform-origin: 0 100%;
  -o-transform-origin: 0 100%;
  transform-origin: 0 100%;
}

#heart:after {
  left: 0;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transform-origin: 100% 100%;
  -moz-transform-origin: 100% 100%;
  -ms-transform-origin: 100% 100%;
  -o-transform-origin: 100% 100%;
  transform-origin: 100% 100%;
}

body {
  background: #6aa2f7;
}

.decor {
  position: relative;
  max-width: 400px;
  margin: 50px auto 0;
  background: white;
  border-radius: 30px;
}

.form-left-decoration, .form-right-decoration {
  content: "";
  position: absolute;
  width: 70px;
  height: 20px;
  background: #6aa2f7;
  border-radius: 20px;
}

.form-left-decoration {
  bottom: 60px;
  left: -30px;
}

.form-right-decoration {
  top: 60px;
  right: -30px;
}

.form-left-decoration:before, .form-left-decoration:after {
  content: "";
  position: absolute;
  width: 50px;
  height: 20px;
  border-radius: 40px;
  background: rgb(255, 255, 255);
}

.form-right-decoration:before, .form-right-decoration:after {
  content: "";
  position: absolute;
  width: 100px;
  height: 20px;
  border-radius: 100px;
  background: rgb(255, 255, 255);
}

.form-left-decoration:before {
  top: -90px;
  left: -14px;
}

.form-left-decoration:after {
  top: 20px;
  left: 31px;
}

.form-right-decoration:before {
  top: -20px;
  right: -15px;
}

.form-right-decoration:after {
  top: 15px;
  left: -100px;
}

.circle {
  position: absolute;
  bottom: 80px;
  left: -500px;
  width: 390px;
  height: 390px;
  border-radius: 50%;
  background: rgb(250, 250, 250);
}

.form-inner {
  padding: 50px;
}

.form-inner input, .form-inner textarea {
  display: block;
  width: 100%;
  padding: 0 20px;
  margin-bottom: 10px;
  background: #E9EFF6;
  line-height: 40px;
  border-width: 0;
  border-radius: 20px;
  font-family: 'Roboto', sans-serif;
}

.sex {
  display: flex;
}

.rul {
  display: inline-block;
}

.form-inner input[type="submit"] {
  margin-top: 30px;
  background: #3e7bd8;
  border-bottom: 4px solid #6aa2f7;
  color: white;
  font-size: 14ix;
}

.form-inner textarea {
  resize: none;
}

.form-inner h3 {
  margin-top: 0;
  font-family: 'Roboto', sans-serif;
  font-weight: 500;
  font-size: 24px;
  color: #707981;
}
</style>

<?php
foreach ($c['#content'] as $content) {
  echo $content;
}
?>

