<html lang="ru">

<head>
  <title>Form</title>
  <meta charset="UTF-8" />
  <style>
    .error {
      border: 2px solid red;
    }
  </style>
</head>

<body>
  <form class="decor1" action="" method="post">
    <div class="form-left-decoration"></div>
    <div class="form-right-decoration"></div>
    <div class="circle">
      <div id="heart"></div>
      <div id="infinity"></div>
    </div>

    <div class="form-inner">

      <h3><?php print($vhod); ?></h3>
      <?php

      if (!empty($messages_fio)) {
        print('<div id="messages">');
        print($messages_fio);
        print('</div>');
      }
      ?>
      <?php
      if (!empty($_COOKIE['save']))
        print($save);
      print('<br>');
      print('<br>');
      if (!empty($_COOKIE['pass'])) {
        print('логин <strong>' . strip_tags($_COOKIE['login']) . '</strong>
        и пароль <strong>' . strip_tags($_COOKIE['pass']) . '</strong> ');
      }
      ?>
      <div <?php if ($errors['fio'] || $errors['Wfio']) {
              print 'class="error"';
            } ?>>
        <input type="text" placeholder="Имя" name="fio" value="<?php print $values['fio']; ?>">
      </div>

      <?php
      if (!empty($messages_email)) {
        print('<div id="messages">');
        print($messages_email);
        print('</div>');
      } ?>
      <div <?php if ($errors['email']) {
              print 'class="error"';
            } ?>>
        <input type="text" placeholder="Email" name="email" value="<?php print $values['email']; ?>">
      </div>
      <?php
      if (!empty($messages_date)) {
        print('<div id="messages">');
        print($messages_date);
        print('</div>');
      }
      ?>
      <input type="text" placeholder="Дата" id="date" name="date" value="<?php print $values['date']; ?>" />
      <h4>Пол:</h4>
      <?php
      if (!empty($messages_sex)) {
        print('<div id="messages">');
        print($messages_sex);
        print('</div>');
      }
      ?>
      <div class="sex">
        <label>
          <input type="radio" name="sex" value="male" <?php if ($values['sex'] == 'male') print('checked') ?>>Мужской &nbsp;</label>
        <label>
          <input type="radio" name="sex" value="female" <?php if ($values['sex'] == 'female') print('checked') ?>>&nbsp; Женский </label>
      </div>
      <h4> Количество конечностей:</h4>
      <?php
      if (!empty($messages_some)) {
        print('<div id="messages">');
        print($messages_some);
        print('</div>');
      }
      ?>
      <div class="sex">
        <label>
          <input type="radio" name="some" value="1" <?php if ($values['some'] == '1') print('checked') ?>>одна&nbsp;</label>
        <label>
          <input type="radio" name="some" value="2" <?php if ($values['some'] == '2') print('checked') ?>>&nbsp;две</label>

        <label>
          <input type="radio" name="some" value="3" <?php if ($values['some'] == '3') print('checked') ?>>&nbsp;три</label>
        <label>
          <input type="radio" name="some" value="4" <?php if ($values['some'] == '4') print('checked') ?>>&nbsp;четыре</label>
        <label>
          <input type="radio" name="some" value="5" <?php if ($values['some'] == '5') print('checked') ?>>&nbsp;пять</label>
        <label>
          <input type="radio" name="some" value="6" <?php if ($values['some'] == '6') print('checked') ?>>&nbsp;шесть</label>
      </div>

      <br><?php
          if (!empty($messages_abilities)) {
            print('<div id="messages">');
            print($messages_abilities);
            print('</div>');
          }
          ?>
      <div>
        <select size="4" multiple name="abilities[]">
          <option disabled>суперсила </option>
          <option value="idclip" <?php if (stripos($values['abilities'], "clip")) print('selected');  ?>> Бесформенность</option>
          <option value="god" <?php if (stripos($values['abilities'], "god")) print('selected');  ?>>Бессмертие</option>
          <option value="Levitation" <?php if (stripos($values['abilities'], "Levitation")) print('selected');  ?>>Полет</option>
        </select>
      </div>
      <br>
      <?php
      if (!empty($messages_about)) {
        print('<div id="messages">');
        print($messages_about);
        print('</div>');
      }
      ?>
      <div>
        <textarea name="about" placeholder="о себе..." rows="3"><?php print($values['about']); ?></textarea>
      </div>
      <?php
      if (!empty($messages_accept)) {
        print(' <div id="messages" >');
        print($messages_accept);
        print('</div>');
      }
      ?>
      <div class="rul">
        <label> <input type="checkbox" name="accept" <?php if ($values['accept'] == 'on') print('checked'); ?>>С политикой формы согласен </label></div>
      <input type="submit" value="Отправить">
      <input type="button" onClick=" 
      <?php
      if ($auto === 1) {

        print('window.location= \' destroy_sess \'');
      } else print('window.location= \' login \' '); ?>" value=" 
      <?php if ($auto === 1) print('Выйти');
      else print('Вход');
      ?>">
      <input type="button" onClick="window.location='admin'" value="Вход администратора">
    </div>
  </form>
</body>

</html>