<?php
session_start();


if (!empty($_SESSION['log'])) {
  header('Location: ./');
}

function login_get($request)
{
  ?>
  <head>
  <title>Login</title>
  <meta charset="UTF-8" />
  <link rel="stylesheet" href="main.css">
  <style>
    .error {
      border: 2px solid red;
    }
  </style>
</head>

<form class="decor1" action="" method="post">
  <div class="form-left-decoration"></div>
  <div class="form-right-decoration"></div>
  <div class="circle">
    <div id="heart"></div>
    <div id="infinity"></div>
  </div>

  <div class="form-inner">
    <h3>Добро пожаловать</h3>
    <?php if (!empty($_COOKIE['error'])) {
      setcookie('error', '', 100000);
      print('<div id="error"><strong>неверный логин или пароль!</strong></div>');
    }

    ?>
    <div <?php if (!empty($_COOKIE['error'])) {
            setcookie('error', '', 100000);
            print 'class="error"';
          }
          ?>>
      <input type="text" placeholder="Login" name="login" value="">
      <input type="password"  maxlength="8" placeholder="password" name="pass" value=""></div>
    <input type="submit" value="Войти">
    <input type="button" onclick="window.location= './'" value="Назад">
  </div>
</form>
<?php
return " ";
}

// Обработчик запросов методом POST.
function login_post($request)
{
  $db = new PDO(
    'mysql:host=localhost;dbname=u16364',
    conf('db_user'),
    conf('db_psw'),
    array(PDO::ATTR_PERSISTENT => true)
  );
  $_SESSION['log'] = '~';
  $stmt = $db->prepare('SELECT id, login FROM application WHERE login = ? AND password = ?');
  $stmt->execute([$_POST['login'], md5($_POST['pass'])]);
  while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
    $_SESSION['log'] = strip_tags($row['login']);
    $_SESSION['uid'] = $row['id'];
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    return redirect('new/nr6/framework');
  }

  if ($_SESSION['log'] == '~') {
    setcookie('error', '1',  time() + 24 * 60 * 60);
    session_destroy();
    return redirect('new/nr6/framework/login');
  }
}

?>
