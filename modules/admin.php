<?php
ob_start();
// Обработчик запросов методом GET.
function admin_get($request)
{
  $db = new PDO(
    'mysql:host=localhost;dbname=u16364',
    conf('db_user'),
    conf('db_psw'),
    array(PDO::ATTR_PERSISTENT => true)
  );
?>

  <head>
    <title>Admin</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="./css/main copy.css">
  </head>

  <form class="decor" action="" method="POST">

    <div class="form-inner">
      <div style="overflow-x:auto;">
        <input type="hidden" name="token" value="45f6d1b23fd1b56d5v456fg45fd31b35f4d56d53g2df456">
        <?php
        $stmt = $db->prepare('SELECT id, name, email, date, sex, amount_of_legs, ability_god, ability_indoor, ability_levitation, about, login FROM application');
        $stmt->execute();
        print "<table>";
        print "<tr><td class=\"b\" align='center'>id</td><td class=\"b\" align='center'>Имя</td><td class=\"b\" align='center'>Почта</td><td class=\"b\" 
        align='center'>Дата</td><td class=\"b\" align='center'>Пoл</td><td class=\"b\" align='center'>Кол-во конечностей</td><td class=\"b\" 
        align='center'>Бессмертие</td><td class=\"b\" align='center'>Бесформенность</td><td class=\"b\" align='center'>Полет</td><td class=\"b\" 
        align='center'>Биография</td><td class=\"b\" align='center'>Login</td></tr>";
        print "<tr>";
        while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
          for ($i = 0; $i < 11; ++$i) {
            $string= "<td class=\"b\" align='center'>".strip_tags($row[$i])."</td>";
            print $string;
          }
        ?>

          <td class="/b">
            <button type="submit" name='id_num' value="<?php print($row[0]); ?>">X</button>
          </td>

        <?php
          print "</tr>";
        }
        print "</table>";
        ?>
      </div>
      <input type="button" onclick=" window.location= 'adm_log_out'" value="Назад">
    </div>
  </form>

<?php
  return " ";
}

// Обработчик запросов методом POST.
function admin_post($request)
{
  $db = new PDO(
    'mysql:host=localhost;dbname=u16364',
    conf('db_user'),
    conf('db_psw'),
    array(PDO::ATTR_PERSISTENT => true)
  );
    try{
      $stmt = $db->prepare("SELECT token FROM admin WHERE token = ? ");
      $stmt->execute(array($_POST['token']));
      while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
        $stmt = $db->prepare("DELETE FROM application WHERE id = ? ");
        $stmt->execute(array($_POST['id_num']));
      }
      
    }catch (PDOException $err) {
      print('<script>alert(\'попробуйте еще раз!\')</script>');
      exit();
    }
   
    return redirect('new/nr6/framework/admin');
}
